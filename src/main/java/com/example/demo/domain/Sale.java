package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "sales")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int slNo;
    private int salesmanId;
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ProID", nullable = false)
    private Product product;
    private String salesmanName;
    private long dos;

    public Sale() {
    }

    public Sale(int salesmanId, Product product, String salesmanName, long dos) {
        this.salesmanId = salesmanId;
        this.product = product;
        this.salesmanName = salesmanName;
        this.dos = dos;
    }

    public int getSlNo() {
        return slNo;
    }

    public void setSlNo(int slNo) {
        this.slNo = slNo;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public long getDos() {
        return dos;
    }

    public void setDos(long dos) {
        this.dos = dos;
    }
}
